import os

class Config(object):

    def __init__(self):
        pass

    @staticmethod
    def get(option):
        appdir = os.environ.get('PLEX_REQUESTS_DIR')
        config = {
            'db': './database/db.sqlite', 
            'secret': 'ggJMlvuGItsmA5n8iuk38iMSCtmDOnGG8iIjd8e',
            'appdir': appdir
        }

        if option == 'all':
            return config
        elif option in config:
            return config[option]
        else:
            return False
