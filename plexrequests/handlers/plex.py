import logging
import base64
import requests
from xml.etree import ElementTree
from database.models import PlexUser, Settings


logger = logging.getLogger(__name__)

def plex_authentication(username, password):
    """
    Handle user login and registrations. Refer to issue #2 (https://github.com/lokenx/plex_requests/issues/2) for
    logic behind this

    :param username: user supplied username
    :param password: user supplied password
    :return: valid user object or None
    """
    try:
        # Check if user is an existing user
        user = PlexUser.get(PlexUser.username == username)
        if user:
            if user.check_password(password):
                return True
            else:
                logger.warn("User {} entered an incorrect password".format(user))
                return None
    except Exception:
        # If not an existing user check with Plex
        base64_auth = base64.encodestring(('%s:%s' % (username, password)).encode()).decode().replace('\n', '')
        logger.warn(base64_auth)
        headers = {
            'Authorization': 'Basic ' + base64_auth,
            'X-Plex-Client-Identifier':'BLS6IF8NV9OX3LHGIG9G63WIGO',
            'X-Plex-Version': '0.1.0',
            'X-Plex-Platform': 'Python',
            'X-Plex-Device-Name': 'Plex Requests'
        }
        check_plex_login = requests.post('https://plex.tv/users/sign_in.json', headers=headers)

        if check_plex_login.status_code == 201:
            email = check_plex_login.json()['user']['email']
            token = Settings.get().plex_token
            friends_list = requests.get('https://plex.tv/pms/friends/all?X-Plex-Token=%s' % token)

            # If valid user confirm they're on the friends list of server admin
            if friends_list.status_code == 200:
                tree = ElementTree.fromstring(friends_list.text)
                for user in tree.findall('User'):
                    if username == user.get('username'):
                        return {
                            'username': username,
                            'password': password,
                            'email': email
                        }

        # If not a valid user or not on the friends list return None
        message = """The user {} attempted to login, but failed Plex.tv verification
            or isn't on your friends list""".format(username)
        logger.warn(message + " " + username + " " + password)
        return None
