import logging
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
from config import Config as conf
#from plexrequests import server
from passlib.hash import sha256_crypt
db_file = conf.get('db')

if db_file != False:
    db = SqliteExtDatabase(db_file)
else:
    logger.warn(dbfile + ": Not a valid path")

class BaseModel(Model):
    class Meta:
        database = db

class PlexUser(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    email = CharField()
    role = CharField()

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password)

    class Meta:
        order_by = ('username',)


class Settings(BaseModel):
    plex_token = CharField(default='abcd1234')

class Database(BaseModel):
    def db_connect(self, db):
        self.db = db
        return db

def create_tables():
    db.connect()
    db.create_tables([PlexUser, Settings], True)
    Settings.create()


create_tables()
