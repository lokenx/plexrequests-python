import logging
import tornado.ioloop
import tornado.web
from config import Config as conf
import handlers.authentication
import handlers.search
import handlers.settings

from tornado.options import options, define, parse_command_line

class Server(object):
  
    secret = conf.get('secret')
  
    def __init__(self):
        pass

    def check_config(config):
        ## TODO Create logic to test if config values are sane
        pass

    def start(self):
        define('port', type=int, default=8000)
        define('url', type=str, default="")

        parse_command_line()
        app = tornado.web.Application([
          (r"" + options.url, handlers.search.SearchHandler),
          (r"" + options.url + "/login", handlers.authentication.LoginHandler),
          (r"" + options.url + "/settings", handlers.settings.SettingsHandler),
        ], debug=True, cookie_secret=self.secret, login_url=options.url + "/login", template_path='templates')

        app.listen(options.port)

        logger = logging.getLogger(__name__)
        logger.info('Server started at http://localhost:{}{}'.format(
          options.port, options.url))

        try:
            tornado.ioloop.IOLoop.instance().start()
        except KeyboardInterrupt:
            tornado.ioloop.IOLoop.instance().stop()
            logger.info('Server stopped')


