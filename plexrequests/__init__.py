import os, sys
import config

# Check if the packages directory path is in sys.path. If not, we will append the projects current directory to syspath
# which will make the process of importing modules from the package the same as if the package was installed in pythons
# package directory.

PLEX_REQUESTS_DIR = os.path.abspath('../')

if PLEX_REQUESTS_DIR in sys.path:
  os.environ['PLEX_REQUESTS_DIR'] = PLEX_REQUESTS_DIR
else:
  sys.path.append(PLEX_REQUESTS_DIR)


__all__ = ['server', 'main', 'config', 'tests']
