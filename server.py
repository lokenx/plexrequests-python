import logging
import tornado.ioloop
import tornado.web

import handlers.authentication
import handlers.search
import handlers.settings

from tornado.options import options, define, parse_command_line


DATABASE = 'db.sqlite'
SECRET = 'ggJMlvuGItsmA5n8iuk38iMSCtmDOnGG8iIjd8eP'


def main():
    define('port', type=int, default=8000)
    define('url', type=str, default="")

    parse_command_line()

    app = tornado.web.Application([
        (r"" + options.url, handlers.search.SearchHandler),
        (r"" + options.url + "/login", handlers.authentication.LoginHandler),
        (r"" + options.url + "/settings", handlers.settings.SettingsHandler),
    ], debug=True, cookie_secret=SECRET, login_url=options.url + "/login", template_path='templates')

    app.listen(options.port)

    logger = logging.getLogger(__name__)
    logger.info('Server started at http://localhost:{}{}'.format(
        options.port, options.url))

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
        logger.info('Server stopped')


if __name__ == "__main__":
    main()
