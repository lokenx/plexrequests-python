import tornado.web
from .base import BaseHandler


class SearchHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.write('Search Page')