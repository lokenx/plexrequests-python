import logging

from peewee import PeeweeException
from database.models import PlexUser, Settings, database
from passlib.hash import sha256_crypt
from tornado.options import options

from .base import BaseHandler
from .plex import *

logger = logging.getLogger(__name__)


def create_account(user):
    if not user['username'] or not user['password']:
        raise PeeweeException('Missing username or password')

    password_hash = sha256_crypt.encrypt(user['password'])
    try:
        with database.transaction():
            PlexUser.create(
                username=user['username'],
                password=password_hash,
                email=user['email'],
                role='user')
    except PeeweeException as error:
        logger.error(error)
        raise PeeweeException


class LoginHandler(BaseHandler):
    def get(self):
        if self.get_current_user():
            self.redirect(self.get_argument('next', '/'))
        self.render('login.html')

    def post(self):
        try:
            plex_auth = plex_authentication(self.get_argument("username"), self.get_argument("password"))
            if plex_auth is True:
                self.set_secure_cookie("user", self.get_argument("username"))
                self.redirect(self.get_argument('next', '/'))
            elif plex_auth is not None:
                new_user = create_account(plex_auth)
                self.set_secure_cookie("user", self.get_argument("username"))
                self.redirect(self.get_argument('next', '/'))
            else:
                self.redirect(options.url + "/login")
        except Exception as error:
            logger.error(error)
            self.redirect(options.url + "/login")
