import tornado.web
import database.models

from .base import BaseHandler


class SettingsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        conf = database.models.Settings.get()
        self.render("settings.html", settings=conf)

    @tornado.web.authenticated
    def post(self):
        config = database.models.Settings.get()
        config.plex_token = self.get_argument("plex_token")
        config.save()
        self.redirect("/settings")