from peewee import *
from server import DATABASE
from passlib.hash import sha256_crypt

database = SqliteDatabase(DATABASE)


class BaseModel(Model):
    class Meta:
        database = database


class PlexUser(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    email = CharField()
    role = CharField()

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password)

    class Meta:
        order_by = ('username',)


class Settings(BaseModel):
    plex_token = CharField(default='abcd1234')


def create_tables():
    database.connect()
    database.create_tables([PlexUser, Settings], True)
    Settings.create()

create_tables()