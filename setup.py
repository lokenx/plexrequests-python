# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='plexrequests',
    version='0.0.1',
    description='Request media for download to plex media server',
    long_description=readme,
    author='lokenx',
    author_email='rigrassm@gmail.com',
    url='https://github.com/rigrassm/plexrequests-python',
    license=license,
    packages=find_packages(exclude=('tests'))
)

